FROM openjdk:8u275-jdk

ADD target/*.jar app.jar
CMD ["java", "-jar", "app.jar"]
EXPOSE 8080